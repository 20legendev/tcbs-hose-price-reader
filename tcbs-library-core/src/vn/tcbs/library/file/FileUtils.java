package vn.tcbs.library.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.CodeSource;

public class FileUtils {

	private long timeStamp;

	public String readFile(String filePath) {
		String data = "";
		BufferedReader br = null;
		FileReader inputFile;
		try {
			inputFile = new FileReader(filePath);
			br = new BufferedReader(inputFile);
			String line;
			while ((line = br.readLine()) != null) {
				data += line;
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return data;
	}

	public static String getAbsolutePath() {
		CodeSource codeSource = FileUtils.class.getProtectionDomain()
				.getCodeSource();
		try {
			File jarFile = new File(codeSource.getLocation().toURI().getPath());
			String jarDir = jarFile.getParentFile().getPath();
			return jarDir;
		} catch (Exception e1) {
			return null;
		}
	}
}
