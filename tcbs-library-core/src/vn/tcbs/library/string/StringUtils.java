package vn.tcbs.library.string;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

	public static String[] splitString(String string, int length) {
		int len = (int) Math.ceil((float) string.length() / length);
		String[] data = new String[len];
		String chunck = "";
		Matcher matcher = Pattern.compile(".{0," + length + "}")
				.matcher(string);
		int count = 0;
		while (matcher.find()) {
			if (matcher.start() != matcher.end()) {
				chunck = string.substring(matcher.start(), matcher.end());
				data[count] = chunck;
				count++;
			}
		}
		return data;
	}
}
