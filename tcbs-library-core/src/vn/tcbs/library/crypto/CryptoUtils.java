package vn.tcbs.library.crypto;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CryptoUtils {

	private static String tokenSaltKey = "TCBS_FLYING_HIGH_2015";

	public static String tokenGenerator(String email)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String token = email + File.separator + System.currentTimeMillis()
				+ File.separator + tokenSaltKey;
		System.out.println(token);
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		md.update(token.getBytes("utf-8"));
		byte[] encoded = md.digest();

		return convToHex(encoded);
	}

	private static String convToHex(byte[] data) {
		StringBuilder buf = new StringBuilder();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	public static String encryptPassword(String password, String saltKey) {
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			String pwd = password + "/" + saltKey;
			byte[] hash = digest.digest(pwd.getBytes("UTF-8"));
			return convToHex(hash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

}
