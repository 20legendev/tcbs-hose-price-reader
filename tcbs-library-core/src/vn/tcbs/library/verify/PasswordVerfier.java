package vn.tcbs.library.verify;

public class PasswordVerfier {
	public static Integer verify(String password) {
		if (password.indexOf(" ") >= 0) {
			return 1;
		}
		if (password.length() < 6) {
			return 2;
		}
		return 0;
	}
}
