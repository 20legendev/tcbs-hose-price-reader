var config = require("./config");
var path = require("path");

Bootstrap = {
	init : function(app) {
		console.log("Bootstrap init...");
		this.app = app;
		this.modules = {};
		this.initModules();
		this.initSendFiles(app);
	},

	initModules : function() {
		var mods = config.module;
		for (var i = 0; i < mods.length; i++) {
			this.modules[mods[i]] = require("../module/" + mods[i] + "/index");
			this.modules[mods[i]].init();
		}
	},

	sendFiles : function(str, strtype, link) {
		this.app = app;
		app.get('/' + link, function(req, res){
			res.setHeader("Content-Type", strtype);
			res.sendFile(str);
		});
	},

	initSendFiles : function(app) {
		app.get('/', function(req, res){res.sendFile(path.resolve(__dirname + '../../../index.html'))});

		var files = config.files;
		for (var i = 0; i < files.length; i++) {
			var str = path.resolve(__dirname + '../../../' + files[i]);

			var strtype = str.substring(str.lastIndexOf('.')+1,str.length);
			if (strtype === 'css') strtype = 'text/css';
			if (strtype === 'js') strtype = 'application/javascript';
			this.sendFiles(str, strtype, files[i]);
		} 
	}
};

module.exports = Bootstrap;