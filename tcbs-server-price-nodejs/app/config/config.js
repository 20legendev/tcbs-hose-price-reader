Config = {
	module : [ "route", "pricereader", "web"],
	files : ["css/banggia/vnds_banggia.css", "js/addStock.js", "js/editStock.js","js/standardlizeStock.js", "js/convertObj.js"],
	controller : {
		"priceController" : "web/controller/PriceController"
	},
	queue : {
		uri : "amqp://localhost",
		price_queue : "tcbs.price.securities"
	}
};

module.exports = Config;