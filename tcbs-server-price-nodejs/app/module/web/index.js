var priceController = require('./controller/PriceController');

Web = {
	init : function(app) {
		this.app = app;
	},

	sendMessage : function(msg) {
		priceController.init(msg);
	}
};

module.exports = Web;