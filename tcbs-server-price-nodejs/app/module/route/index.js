Route = {
	init : function(app) {
		this.app = app;
		this.controllers = {};
		var controllers = Config.controller;
		for ( var name in controllers) {
			this.controllers[name] = require("../" + controllers[name]);
		}
		this.handle();
	},

	handle : function() {

	}
};
module.exports = Route;