var queueApi = require('amqplib/callback_api');

QueueConsume = {
	init : function(callback) {
		console.log("Queue connecting...");
		var _self = this;
		this.config = {
			priceQueue : global.config.queue.price_queue
		};
		queueApi.connect(global.config.queue.uri, function(err, conn) {
			if (err != null)
				_self.bail(err);
			_self.conn = conn;
			_self.consumer(function (err, msg) {
				if (err != null) 
					_self.bail(err);
				return callback(msg);
			});
		});
	},

	consumer : function(callback) {
		var _self = this;
		function on_open(err, ch) {
			console.log("OPEN queue...");
			if (err != null)
				return callback(err, null);
			ch.assertQueue(_self.config.priceQueue);
			ch.consume(_self.config.priceQueue, function(msg) {
				if (msg !== null) {
					return callback(null, msg);
				}
			});	
		}
		var ok = this.conn.createChannel(on_open);
	},

	bail : function(err) {
		console.error(err);
		process.exit(1);
	}

};

module.exports = QueueConsume;
