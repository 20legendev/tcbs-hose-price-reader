var queueConsume = require('./code/queue-consume');
var Web = require('../web/index');

PriceReader = {
	init : function(app, callback) {
		this.app = app;
		queueConsume.init(function (msg) {
			Web.sendMessage(msg);
		});
	}
};

module.exports = PriceReader;