 var delayTime = 2000; //miliseconds

 function changeBGColor(color, td, data) {
    $(td).css({'color': color, 'background-color': 'yellowgreen'});   
    $(td).html(data);   
    setTimeout(function() {
      $(td).css('background-color', 'black');   
      },delayTime);    
  }

  function editStock(obj) {
    var ob = [obj.stockSymbol,'',obj.ceiling,obj.floor,'',obj.best3Bid,obj.best3BidVolume,obj.best2Bid,obj.best2BidVolume,obj.best1Bid,obj.best1BidVolume,'','','',obj.best3Offer,obj.best3OfferVolume,obj.best2Offer,obj.best2OfferVolume,obj.best1Offer,obj.best1OfferVolume,'','','','','','',''];
    var td;
    for (var i=1;i<27;i++) {
      td = '#' + ob[0] + i.toString();
      if (parseFloat($(td).html()) > parseFloat(ob[i])) {
          changeBGColor('red', td, ob[i]);
      }

      else if (parseFloat($(td).html()) < parseFloat(ob[i])) {
          changeBGColor('lime', td, ob[i]);
      }
    }

  }