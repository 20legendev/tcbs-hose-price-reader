application = (function() {
	var express = require('express'),
		bodyParser = require("body-parser"),
		config = global.config = require("./app/config/config"),
		bootstrap = require('./app/config/bootstrap');
	
	var app = global.app = module.exports = express(), 
		server = require('http').createServer(app),
		io = global.io = require('socket.io')(server);
	
	app.use(bodyParser.urlencoded({
		extended : false
	}));
	
	bootstrap.init(app);
	server.listen(3000);
}).call(this);