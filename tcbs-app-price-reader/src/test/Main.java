package test;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Main {

	public static void main(String[] args) throws IOException,
			NoSuchAlgorithmException {
		// File dbFile = File.createTempFile("database", "db");
		// System.out.println(dbFile);

		// Float a = 2.0f;
		// Float b = 2.0f;
		// System.out.println(a == b);

		// System.out.println(false && false);

		// File file = new File("/home/kiennt/Desktop/test");
		// while (true) {
		// System.out.println(file.lastModified());
		// try {
		// Thread.sleep(1000);
		// } catch (Exception ex) {
		// ex.printStackTrace();
		// }
		// }

		// byte[] c = {0,-104,-106,127};
		// byte[] c = {127, -106, -104, 0};
		// System.out.println(MathUtils.bytesToInt(c));

		MessageDigest md = MessageDigest.getInstance("SHA-1");
		md.reset();
		md.update("kien nguyen".getBytes("UTF-8"));
		byte[] encoded = md.digest();
		System.out.println(encoded.toString());
		System.out.println(convertToHex(encoded));
	}

	private static String convertToHex(byte[] data) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

}
