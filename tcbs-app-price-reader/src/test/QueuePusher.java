package test;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class QueuePusher implements Runnable {
	private final static String QUEUE_NAME = "tcbs.price.securities";
	private Channel channel;
	private String message = "Hello Kien";

	public QueuePusher() {
		ConnectionFactory factory = new ConnectionFactory();
		try {
			factory.setUri("amqp://admin:admin@localhost:5672");
			Connection connection = factory.newConnection();
			this.channel = connection.createChannel();
//			this.channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void run() {
		while (true) {
			try {
				this.channel.basicPublish("", QUEUE_NAME, null,
						this.message.getBytes());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
