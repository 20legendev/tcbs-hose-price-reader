package vn.tcbs.tool.base;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.rabbitmq.client.Channel;

import vn.tcbs.core.app.price.object.MapObject;
import vn.tcbs.core.app.price.service.QueueUtils;

public class TcbsFileUtils {

	public static String readResourceFile(String filePath) throws IOException {
		Resource resource = new ClassPathResource(filePath);
		InputStream is = resource.getInputStream();
		return getStringFromInputStream(is);
	}

	public static String getStringFromInputStream(InputStream is) {
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
		String line;
		try {
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return sb.toString();
	}

	public static List<MapObject> readFormatFile(String formatFile) {
		String data = null;
		List<MapObject> structure = null;
		try {
			data = TcbsFileUtils.readResourceFile("META-INF/file-structure/"
					+ formatFile);
		} catch (Exception ex) {
			ex.printStackTrace();
			return structure;
		}
		if (data != null) {
			String[] structureList = data.split(";");
			structure = new ArrayList<MapObject>();
			MapObject obj;
			String[] tmp;
			HashMap<String, Integer> mapper = new HashMap<String, Integer>();
			mapper.put("I", 0);
			mapper.put("L", 1);
			mapper.put("D", 2);
			mapper.put("S", 3);
			for (int i = 0; i < structureList.length; i++) {
				tmp = structureList[i].split(":");
				obj = new MapObject(tmp[0], mapper.get(tmp[1]),
						Integer.valueOf(tmp[2]), tmp.length == 3 ? true : tmp[3].equals("1") ? true : false);
				structure.add(obj);
			}
		}
		return structure;
	}
	
}