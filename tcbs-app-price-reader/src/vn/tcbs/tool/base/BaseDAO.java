package vn.tcbs.tool.base;

import org.hibernate.Session;

public class BaseDAO {

	public Session getSession() {
		return HibernateUtil.getSessionFactory().getCurrentSession();
	}

};
