package vn.tcbs.tool.base;

import java.io.File;

import vn.tcbs.core.app.price.service.MapDatabase;

public class FileChangeHandler {

	protected Long lastModified;
	protected Long lastPointer;
	private String filePath;
	protected Long timeout;
	protected String keyPointer;
	private boolean isSubcribe = true;
	private String key;
	private Boolean isPointerCheck;
	protected File file;

	public FileChangeHandler() {
	}

	protected void startSubcribe(String filePath, Long timeout, String key,
			Boolean isPointerCheck) {
		this.key = key.equals(null) ? filePath : key;
		this.keyPointer = this.key + ".pointer";
		this.isPointerCheck = isPointerCheck;

		lastModified = Long.valueOf(MapDatabase.get(this.key, "0"));
		lastPointer = getLastDbPointer();
		this.filePath = filePath;
		this.timeout = timeout == null ? 50 : timeout;

		file = new File(filePath);
		while (isSubcribe) {
			try {
				this.check();
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	protected void stopSubcribe() {
		this.isSubcribe = false;
	}

	protected void changeSubcribeFile(String filePath, String key) {
		this.key = key.equals(null) ? filePath : key;
		this.filePath = filePath;
		file = new File(filePath);
	}

	protected void check() {
		Long fileLastModified = file.lastModified();
		if (isModified(fileLastModified)
				|| (this.isPointerCheck && !isReadDone(file.length()))) {
			lastModified = fileLastModified;
			MapDatabase.set(this.key, String.valueOf(lastModified));
			this.fileChange(lastModified);
		}
	}

	protected void fileChange(Long lastModified) {
	}

	protected void setLastDbPointer(Long pointer) {
		lastPointer = pointer;
		MapDatabase.set(this.keyPointer, String.valueOf(lastPointer));
	}

	protected Long getLastDbPointer() {
		return Long.valueOf(MapDatabase.get(this.keyPointer, "0"));
	}

	private Boolean isModified(Long time) {
		if (lastModified == null || !time.equals(lastModified)) {
			return true;
		}
		return false;
	}

	private Boolean isReadDone(Long fileLength) {
		if (fileLength.equals(lastPointer)) {
			return true;
		}
		return false;
	}

}
