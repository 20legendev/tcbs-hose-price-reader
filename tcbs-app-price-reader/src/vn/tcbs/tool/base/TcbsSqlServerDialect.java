package vn.tcbs.tool.base;

import java.sql.Types;

import org.hibernate.HibernateException;
import org.hibernate.dialect.SQLServer2008Dialect;
import org.hibernate.type.StandardBasicTypes;

public class TcbsSqlServerDialect extends SQLServer2008Dialect {

	public TcbsSqlServerDialect() {
		registerColumnType(Types.NVARCHAR, 4000, "nvarchar($l)");
		registerColumnType(Types.NVARCHAR, "nvarchar(max)");
		registerColumnType(Types.NVARCHAR, "ntext");

		registerHibernateType(Types.NCHAR,
				StandardBasicTypes.CHARACTER.getName());
		registerHibernateType(Types.LONGNVARCHAR,
				StandardBasicTypes.TEXT.getName());
		registerHibernateType(Types.NVARCHAR,
				StandardBasicTypes.STRING.getName());
		registerHibernateType(Types.NCLOB, StandardBasicTypes.CLOB.getName());
	}

	public String getTypeName(int code, int length, int precision, int scale)
			throws HibernateException {
		if (code != 2005) {
			return super.getTypeName(code, length, precision, scale);
		} else {
			return "ntext";
		}
	}
}
