package vn.tcbs.core.app.price.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import vn.tcbs.core.app.price.model.SecurityPrice;
import vn.tcbs.tool.base.BaseDAO;

public class SecurityPriceDAOImpl extends BaseDAO implements SecurityPriceDAO {

	@Override
	public void addNew(SecurityPrice sp) {
		Session session = getSession();
		Transaction tx = session.beginTransaction();
		session.save(sp);
		tx.commit();
		System.out
				.println("save to database " + Thread.currentThread().getId());
	}

}
