package vn.tcbs.core.app.price.job;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import vn.tcbs.core.app.price.object.MapObject;
import vn.tcbs.core.app.price.service.QueueUtils;
import vn.tcbs.library.math.MathUtils;

public class SecurityReader extends FileReaderJob implements Runnable {

	private HashMap<Short, byte[]> map;

	public SecurityReader(String fileName, String queueName,
			Integer dataLength, String formatFile, Long timeOut) {
		super(fileName, queueName, dataLength, formatFile, timeOut);
		map = new HashMap<Short, byte[]>();
	}

	@Override
	protected void fileChange(Long lastModified) {
		
		
		try {
			Long start = new Date().getTime();
			DataInputStream input = new DataInputStream(new FileInputStream(
					file));
			String tmp;
			Short code = 0;
			while (input.available() > 0) {
				byte[] block = new byte[this.dataLength];
				input.read(block);
				code = MathUtils.bytesToShort(Arrays.copyOfRange(block, 0, 2));
				if (!map.containsKey(code)
						|| !Arrays.equals(map.get(code), block)) {
					tmp = fromBytes(block);
					String[] f = tmp.split("|");
//					if (!f[9].equalsIgnoreCase("s")
//							&& !f[10].equalsIgnoreCase("d")) {
						QueueUtils.writeToQueue(exchangeName, tmp.getBytes());
						map.put(code, block);
//					}
						System.out.println(tmp);
				}
			}
			
			// xem co ma nao bi dung, bi xoa -> xoa di khoi redis
			
			
			// cho phep ket noi redis
			//jredis, jedis....
			
			
			
			input.close();
			System.out.println(new Date().getTime() - start);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
