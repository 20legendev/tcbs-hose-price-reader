package vn.tcbs.core.app.price.job;

import java.util.ArrayList;
import java.util.List;

import vn.tcbs.core.app.price.service.DataPath;
import vn.tcbs.core.app.price.service.QueueUtils;
import vn.tcbs.tool.base.FileChangeHandler;

public class CentralManager extends FileChangeHandler implements Runnable {

	private List<FileReaderJob> list;

	public CentralManager() {
		super();
		list = new ArrayList<FileReaderJob>();
	}

	public void run() {
		this.startSubcribe(new DataPath().getDataLocation() + "datapath.map",
				1000L, "datapath.map", false);
		try {
			QueueUtils.subcribeControlChange();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	protected void fileChange(Long lastModified) {
		for (int i = 0; i < list.size(); i++) {
			list.get(i).pathChange();
		}
	}

	public void addListener(FileReaderJob frj) {
		list.add(frj);
	}

}
