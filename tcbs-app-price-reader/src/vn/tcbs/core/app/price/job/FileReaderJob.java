package vn.tcbs.core.app.price.job;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import vn.tcbs.core.app.price.object.MapObject;
import vn.tcbs.core.app.price.service.DataPath;
import vn.tcbs.core.app.price.service.QueueUtils;
import vn.tcbs.library.math.MathUtils;
import vn.tcbs.tool.base.FileChangeHandler;
import vn.tcbs.tool.base.TcbsFileUtils;

public class FileReaderJob extends FileChangeHandler implements Runnable {

	protected String fileName;
	private String formatFile;
	protected String queueName;
	protected String exchangeName;
	protected Integer dataLength;
	protected List<MapObject> structure;
	protected String filePath;
	private String keyModified;
	private Long timeout;

	public FileReaderJob(String fileName, String queueName, Integer dataLength,
			String formatFile, Long timeout) {
		this.dataLength = dataLength;
		this.fileName = fileName;
		this.queueName = queueName;
		this.formatFile = formatFile;
		this.timeout = timeout;
	}

	public void run() {
		structure = TcbsFileUtils.readFormatFile(formatFile);
		exchangeName = queueName + ".exchange";
		QueueUtils.declareQueue(queueName);
		this.refresh();

		this.startSubcribe(filePath, 50L, keyModified, false);

		// TODO: test only
		// while (true) {
		// fileChange(0L);
		// try {
		// Thread.sleep(thiFs.timeout);
		// } catch (InterruptedException e) {
		// e.printStackTrace();
		// }
		// }
	}

	public void pathChange() {
		this.refresh();
	}

	private void refresh() {
		String[] folder = new DataPath().getLastFolder();
		filePath = folder[1] + File.separator + this.fileName;
		keyModified = folder[0] + ":" + fileName + ".lastmodified";
		keyPointer = keyModified + ".pointer";
		lastPointer = getLastDbPointer();
		this.changeSubcribeFile(filePath, keyModified);
	}

	@Override
	protected void fileChange(Long lastModified) {
		try {
			DataInputStream input = new DataInputStream(new FileInputStream(
					file));
			// TODO test only
			// input.skip(lastPointer);
			String tmp;
			while (input.available() > 0) {
				byte[] block = new byte[this.dataLength];
				input.read(block);
				tmp = fromBytes(block);
				QueueUtils.writeToQueue(exchangeName, tmp.getBytes());
				lastPointer += this.dataLength;
				setLastDbPointer(lastPointer);
			}
			input.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected String fromBytes(byte[] b) {
		int byteCounter = 0;
		MapObject object;
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < structure.size(); i++) {
			object = structure.get(i);
			switch (object.getType()) {
			case 0:
				sb.append(MathUtils.bytesToShort(Arrays.copyOfRange(b,
						byteCounter, byteCounter + object.getLen())));
				break;
			case 1:
				sb.append(MathUtils.bytesToInt(Arrays.copyOfRange(b,
						byteCounter, byteCounter + object.getLen())));
				break;
			case 2:
				sb.append(MathUtils.getDouble(Arrays.copyOfRange(b,
						byteCounter, byteCounter + object.getLen())));
				break;
			case 3:
				sb.append(new String(Arrays.copyOfRange(b, byteCounter,
						byteCounter + object.getLen())).replaceAll("\\s+", " ")
						.trim());
				break;
			}
			sb.append("|");
			byteCounter += object.getLen();
		}
		return sb.toString();
	}

}
