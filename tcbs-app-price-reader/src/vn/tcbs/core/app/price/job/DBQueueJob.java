package vn.tcbs.core.app.price.job;

import org.apache.commons.configuration.Configuration;

import vn.tcbs.core.app.price.model.SecurityPrice;
import vn.tcbs.core.app.price.service.ConfigReader;
import vn.tcbs.core.app.price.service.QueueUtils;
import vn.tcbs.core.app.price.service.SecurityServiceImpl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;

public class DBQueueJob implements Runnable {

	private String queueName;
	private ObjectMapper mapper;
	private SecurityServiceImpl securityService;

	public DBQueueJob() {
	}

	public void run() {
		System.out.println("Hello from a db queue!"
				+ Thread.currentThread().getId());

		mapper = new ObjectMapper();
		securityService = new SecurityServiceImpl();
		this.readQueue();
	}

	public void readQueue() {
		Configuration config = ConfigReader
				.getConfig("queue.properties", false);
		queueName = config.getString("queue.stock.price");
		Channel channel = QueueUtils.getChannel();
		if (channel != null) {
			QueueingConsumer consumer = new QueueingConsumer(channel);
			try {
				channel.basicConsume(queueName, false, consumer);
				while (true) {
					QueueingConsumer.Delivery delivery = consumer
							.nextDelivery();
					String message = new String(delivery.getBody());
					SecurityPrice sec = mapper.readValue(message, SecurityPrice.class);
					insertDb(sec);
					
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}else{
			System.out.println("Queue connect failed");
		}
	}

	public void insertDb(SecurityPrice sp) {
		securityService.addNew(sp);
	}
}
