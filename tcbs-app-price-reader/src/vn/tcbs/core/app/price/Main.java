package vn.tcbs.core.app.price;

import org.mapdb.DB;

import vn.tcbs.core.app.price.job.CentralManager;
import vn.tcbs.core.app.price.job.SecurityReader;
import vn.tcbs.core.app.price.service.MapDatabase;

public class Main {

	public static void main(String[] args) {
		// DBQueueJob job = new DBQueueJob();
		// Thread t = new Thread(job);
		// t.start();
		//
		String mode = null;
		if (args.length > 0) {
			mode = args[0];
		}
		DB db = MapDatabase.getConnection(mode);
		if (db == null) {
			return;
		}

		SecurityReader sec = new SecurityReader("SECURITY.DAT",
				"queue.stock.price.security", 221, "security", 50L);
		new Thread(sec).start();
		
		SecurityReader sec2 = new SecurityReader("SECURITY.DAT",
				"queue.stock.price.security", 221, "security", 50L);
		new Thread(sec2).start();
		SecurityReader sec3 = new SecurityReader("SECURITY.DAT",
				"queue.stock.price.security", 221, "security", 50L);
		new Thread(sec3).start();
		SecurityReader sec4 = new SecurityReader("SECURITY.DAT",
				"queue.stock.price.security", 221, "security", 50L);
		new Thread(sec4).start();
		SecurityReader sec5 = new SecurityReader("SECURITY.DAT",
				"queue.stock.price.security", 221, "security", 50L);
		new Thread(sec5).start();
		
		

		// FileReaderJob ls = new FileReaderJob("LS.DAT",
		// "queue.stock.price.ls",
		// 21, "ls");
		// new Thread(ls).start();
		//
		// FileReaderJob putexec = new FileReaderJob("PUT_EXEC.DAT",
		// "queue.stock.price.put_exe", 15, "put_exec");
		// new Thread(putexec).start();
		//
		// FileReaderJob le = new FileReaderJob("LE.DAT",
		// "queue.stock.price.le",
		// 32, "le");
		// new Thread(le).start();

		CentralManager cm = new CentralManager();
		cm.addListener(sec);
		// dbpj.addListener(ls);
		// dbpj.addListener(le);
		// dbpj.addListener(putexec);
		new Thread(cm).start();

	}
}
