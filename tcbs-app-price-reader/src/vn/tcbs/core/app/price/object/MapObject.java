package vn.tcbs.core.app.price.object;

public class MapObject {
	private String name;
	private Integer type;
	private Integer len;
	private Boolean visible;

	public MapObject(String name, Integer type, Integer len, Boolean visible){
		this.name = name;
		this.type = type;
		this.len = len;
		this.visible = visible;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getLen() {
		return len;
	}

	public void setLen(Integer len) {
		this.len = len;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
	

}