package vn.tcbs.core.app.price.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The persistent class for the Le database table.
 * 
 */
@Entity
@NamedQuery(name = "PutExec.findAll", query = "SELECT l FROM Le l")
public class PutExec implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "ConfirmNo")
	private Integer confirmNo;

	@Column(name = "StockNo")
	private Short stockNo;

	@Column(name = "Vol")
	private Integer vol;

	@Column(name = "Price")
	private Integer price;

	@Column(name = "Board")
	private String board;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getConfirmNo() {
		return confirmNo;
	}

	public void setConfirmNo(Integer confirmNo) {
		this.confirmNo = confirmNo;
	}

	public Short getStockNo() {
		return stockNo;
	}

	public void setStockNo(Short stockNo) {
		this.stockNo = stockNo;
	}

	public Integer getVol() {
		return vol;
	}

	public void setVol(Integer vol) {
		this.vol = vol;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getBoard() {
		return board;
	}

	public void setBoard(String board) {
		this.board = board;
	}

	public String toString() {
		ObjectMapper om = new ObjectMapper();
		try {
			return om.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return null;
		}
	}

}