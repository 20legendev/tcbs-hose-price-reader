package vn.tcbs.core.app.price.model;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.sql.Timestamp;

/**
 * The persistent class for the Le database table.
 * 
 */
@Entity
@NamedQuery(name = "Le.findAll", query = "SELECT l FROM Le l")
public class Le implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "AccumulatedVal")
	private Double accumulatedVal;

	@Column(name = "AccumulatedVol")
	private Integer accumulatedVol;

	@Column(name = "Highest")
	private Integer highest;

	@Column(name = "Lowest")
	private Integer lowest;

	@Column(name = "Price")
	private Integer price;

	@Column(name = "Since")
	private Integer since;

	@Column(name = "StockNo")
	private Integer stockNo;

	public Le() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getAccumulatedVal() {
		return accumulatedVal;
	}

	public void setAccumulatedVal(Double accumulatedVal) {
		this.accumulatedVal = accumulatedVal;
	}

	public Integer getAccumulatedVol() {
		return accumulatedVol;
	}

	public void setAccumulatedVol(Integer accumulatedVol) {
		this.accumulatedVol = accumulatedVol;
	}

	public Integer getHighest() {
		return highest;
	}

	public void setHighest(Integer highest) {
		this.highest = highest;
	}

	public Integer getLowest() {
		return lowest;
	}

	public void setLowest(Integer lowest) {
		this.lowest = lowest;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getSince() {
		return since;
	}

	public void setSince(Integer since) {
		this.since = since;
	}

	public Integer getStockNo() {
		return stockNo;
	}

	public void setStockNo(Integer stockNo) {
		this.stockNo = stockNo;
	}

	public String toString() {
		ObjectMapper om = new ObjectMapper();
		try {
			return om.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return null;
		}
	}

}