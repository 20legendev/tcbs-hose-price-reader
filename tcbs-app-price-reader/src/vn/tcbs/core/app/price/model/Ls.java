package vn.tcbs.core.app.price.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The persistent class for the Ls database table.
 * 
 */
@Entity
@NamedQuery(name = "Ls.findAll", query = "SELECT l FROM Ls l")
public class Ls implements Serializable {
	private static final Long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;

	@Column(name = "ConfirmNo")
	private Integer confirmNo;

	@Column(name = "StockNo")
	private Integer stockNo;

	@Column(name = "MatchedVol")
	private Double matchedVol;

	@Column(name = "Price")
	private Integer price;

	@Column(name = "Side")
	private String side;

	public Ls() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getConfirmNo() {
		return confirmNo;
	}

	public void setConfirmNo(Integer confirmNo) {
		this.confirmNo = confirmNo;
	}

	public Integer getStockNo() {
		return stockNo;
	}

	public void setStockNo(Integer stockNo) {
		this.stockNo = stockNo;
	}

	public Double getMatchedVol() {
		return matchedVol;
	}

	public void setMatchedVol(Double matchedVol) {
		this.matchedVol = matchedVol;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public String toString() {
		ObjectMapper om = new ObjectMapper();
		try {
			return om.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return null;
		}
	}

}