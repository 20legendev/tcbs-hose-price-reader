package vn.tcbs.core.app.price.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The persistent class for the security_price database table.
 * 
 */
@Entity
@Table(name = "security_price")
@NamedQuery(name = "SecurityPrice.findAll", query = "SELECT s FROM SecurityPrice s")
public class SecurityPrice implements Serializable {
	private static final Long serialVersionUID = 1L;

	private Short accumulateDeal;

	private Integer avrPrice;

	private String benefit;

	private Integer best1Bid;

	private Integer best1BidVolume;

	private Integer best1Offer;

	private Integer best1OfferVolume;

	private Integer best2Bid;

	private Integer best2BidVolume;

	private Integer best2Offer;

	private Integer best2OfferVolume;

	private Integer best3Bid;

	private Integer best3BidVolume;

	private Integer best3Offer;

	private Integer best3OfferVolume;

	private Short bigDeal;

	private Double bigLotValue;

	private Double bigVal;

	private Integer bigVol;

	private Integer ceiling;

	private String clientIDRequest;

	private Short couponRate;

	private String delist;

	private String designated;

	private Integer floor;

	private String haltResumeFlag;

	private Integer highest;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String issueDate;

	private Integer last;

	private Date lastUpdated;

	private Double lastVal;

	private Integer lastVol;

	private Integer lowest;

	private String matureDate;

	private String meeting;

	private String notice;

	private Short oddDeal;

	private Double oddVal;

	private Integer oddVol;

	private Integer openPrice;

	private Short parValue;

	private String priorCloseDate;

	private Integer priorClosePrice;

	private Integer projectOpen;

	private String sDCFlag;

	private String sectorNo;

	private String securityName;

	private String split;

	private Integer stockNo;

	private String stockSymbol;

	private String stockType;

	private String suspension;

	private Double totalshare;

	private Double totalValue;

	public SecurityPrice() {
	}

	public Short getAccumulateDeal() {
		return this.accumulateDeal;
	}

	public void setAccumulateDeal(Short accumulateDeal) {
		this.accumulateDeal = accumulateDeal;
	}

	public Integer getAvrPrice() {
		return this.avrPrice;
	}

	public void setAvrPrice(Integer avrPrice) {
		this.avrPrice = avrPrice;
	}

	public String getBenefit() {
		return this.benefit;
	}

	public void setBenefit(String benefit) {
		this.benefit = benefit;
	}

	public Integer getBest1Bid() {
		return this.best1Bid;
	}

	public void setBest1Bid(Integer best1Bid) {
		this.best1Bid = best1Bid;
	}

	public Integer getBest1BidVolume() {
		return this.best1BidVolume;
	}

	public void setBest1BidVolume(Integer best1BidVolume) {
		this.best1BidVolume = best1BidVolume;
	}

	public Integer getBest1Offer() {
		return this.best1Offer;
	}

	public void setBest1Offer(Integer best1Offer) {
		this.best1Offer = best1Offer;
	}

	public Integer getBest1OfferVolume() {
		return this.best1OfferVolume;
	}

	public void setBest1OfferVolume(Integer best1OfferVolume) {
		this.best1OfferVolume = best1OfferVolume;
	}

	public Integer getBest2Bid() {
		return this.best2Bid;
	}

	public void setBest2Bid(Integer best2Bid) {
		this.best2Bid = best2Bid;
	}

	public Integer getBest2BidVolume() {
		return this.best2BidVolume;
	}

	public void setBest2BidVolume(Integer best2BidVolume) {
		this.best2BidVolume = best2BidVolume;
	}

	public Integer getBest2Offer() {
		return this.best2Offer;
	}

	public void setBest2Offer(Integer best2Offer) {
		this.best2Offer = best2Offer;
	}

	public Integer getBest2OfferVolume() {
		return this.best2OfferVolume;
	}

	public void setBest2OfferVolume(Integer best2OfferVolume) {
		this.best2OfferVolume = best2OfferVolume;
	}

	public Integer getBest3Bid() {
		return this.best3Bid;
	}

	public void setBest3Bid(Integer best3Bid) {
		this.best3Bid = best3Bid;
	}

	public Integer getBest3BidVolume() {
		return this.best3BidVolume;
	}

	public void setBest3BidVolume(Integer best3BidVolume) {
		this.best3BidVolume = best3BidVolume;
	}

	public Integer getBest3Offer() {
		return this.best3Offer;
	}

	public void setBest3Offer(Integer best3Offer) {
		this.best3Offer = best3Offer;
	}

	public Integer getBest3OfferVolume() {
		return this.best3OfferVolume;
	}

	public void setBest3OfferVolume(Integer best3OfferVolume) {
		this.best3OfferVolume = best3OfferVolume;
	}

	public Short getBigDeal() {
		return this.bigDeal;
	}

	public void setBigDeal(Short bigDeal) {
		this.bigDeal = bigDeal;
	}

	public Double getBigLotValue() {
		return this.bigLotValue;
	}

	public void setBigLotValue(Double bigLotValue) {
		this.bigLotValue = bigLotValue;
	}

	public Double getBigVal() {
		return this.bigVal;
	}

	public void setBigVal(Double bigVal) {
		this.bigVal = bigVal;
	}

	public Integer getBigVol() {
		return this.bigVol;
	}

	public void setBigVol(Integer bigVol) {
		this.bigVol = bigVol;
	}

	public Integer getCeiling() {
		return this.ceiling;
	}

	public void setCeiling(Integer ceiling) {
		this.ceiling = ceiling;
	}

	public String getClientIDRequest() {
		return this.clientIDRequest;
	}

	public void setClientIDRequest(String clientIDRequest) {
		this.clientIDRequest = clientIDRequest;
	}

	public Short getCouponRate() {
		return this.couponRate;
	}

	public void setCouponRate(Short couponRate) {
		this.couponRate = couponRate;
	}

	public String getDelist() {
		return this.delist;
	}

	public void setDelist(String delist) {
		this.delist = delist;
	}

	public String getDesignated() {
		return this.designated;
	}

	public void setDesignated(String designated) {
		this.designated = designated;
	}

	public Integer getFloor() {
		return this.floor;
	}

	public void setFloor(Integer floor) {
		this.floor = floor;
	}

	public String getHaltResumeFlag() {
		return this.haltResumeFlag;
	}

	public void setHaltResumeFlag(String haltResumeFlag) {
		this.haltResumeFlag = haltResumeFlag;
	}

	public Integer getHighest() {
		return this.highest;
	}

	public void setHighest(Integer highest) {
		this.highest = highest;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIssueDate() {
		return this.issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public Integer getLast() {
		return this.last;
	}

	public void setLast(Integer last) {
		this.last = last;
	}

	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Double getLastVal() {
		return this.lastVal;
	}

	public void setLastVal(Double lastVal) {
		this.lastVal = lastVal;
	}

	public Integer getLastVol() {
		return this.lastVol;
	}

	public void setLastVol(Integer lastVol) {
		this.lastVol = lastVol;
	}

	public Integer getLowest() {
		return this.lowest;
	}

	public void setLowest(Integer lowest) {
		this.lowest = lowest;
	}

	public String getMatureDate() {
		return this.matureDate;
	}

	public void setMatureDate(String matureDate) {
		this.matureDate = matureDate;
	}

	public String getMeeting() {
		return this.meeting;
	}

	public void setMeeting(String meeting) {
		this.meeting = meeting;
	}

	public String getNotice() {
		return this.notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	public Short getOddDeal() {
		return this.oddDeal;
	}

	public void setOddDeal(Short oddDeal) {
		this.oddDeal = oddDeal;
	}

	public Double getOddVal() {
		return this.oddVal;
	}

	public void setOddVal(Double oddVal) {
		this.oddVal = oddVal;
	}

	public Integer getOddVol() {
		return this.oddVol;
	}

	public void setOddVol(Integer oddVol) {
		this.oddVol = oddVol;
	}

	public Integer getOpenPrice() {
		return this.openPrice;
	}

	public void setOpenPrice(Integer openPrice) {
		this.openPrice = openPrice;
	}

	public Short getParValue() {
		return this.parValue;
	}

	public void setParValue(Short parValue) {
		this.parValue = parValue;
	}

	public String getPriorCloseDate() {
		return this.priorCloseDate;
	}

	public void setPriorCloseDate(String priorCloseDate) {
		this.priorCloseDate = priorCloseDate;
	}

	public Integer getPriorClosePrice() {
		return this.priorClosePrice;
	}

	public void setPriorClosePrice(Integer priorClosePrice) {
		this.priorClosePrice = priorClosePrice;
	}

	public Integer getProjectOpen() {
		return this.projectOpen;
	}

	public void setProjectOpen(Integer projectOpen) {
		this.projectOpen = projectOpen;
	}

	public String getSDCFlag() {
		return this.sDCFlag;
	}

	public void setSDCFlag(String sDCFlag) {
		this.sDCFlag = sDCFlag;
	}

	public String getSectorNo() {
		return this.sectorNo;
	}

	public void setSectorNo(String sectorNo) {
		this.sectorNo = sectorNo;
	}

	public String getSecurityName() {
		return this.securityName;
	}

	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}

	public String getSplit() {
		return this.split;
	}

	public void setSplit(String split) {
		this.split = split;
	}

	public Integer getStockNo() {
		return this.stockNo;
	}

	public void setStockNo(Integer stockNo) {
		this.stockNo = stockNo;
	}

	public String getStockSymbol() {
		return this.stockSymbol;
	}

	public void setStockSymbol(String stockSymbol) {
		this.stockSymbol = stockSymbol;
	}

	public String getStockType() {
		return this.stockType;
	}

	public void setStockType(String stockType) {
		this.stockType = stockType;
	}

	public String getSuspension() {
		return this.suspension;
	}

	public void setSuspension(String suspension) {
		this.suspension = suspension;
	}

	public Double getTotalshare() {
		return this.totalshare;
	}

	public void setTotalshare(Double totalshare) {
		this.totalshare = totalshare;
	}

	public Double getTotalValue() {
		return this.totalValue;
	}

	public void setTotalValue(Double totalValue) {
		this.totalValue = totalValue;
	}

	public String toString() {
		ObjectMapper om = new ObjectMapper();
		try {
			return om.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return null;
		}
	}

}