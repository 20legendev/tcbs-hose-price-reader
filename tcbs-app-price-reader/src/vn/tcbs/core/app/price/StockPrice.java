package vn.tcbs.core.app.price;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import vn.tcbs.core.app.price.model.Le;
import vn.tcbs.core.app.price.model.Ls;
import vn.tcbs.core.app.price.model.PutExec;
import vn.tcbs.core.app.price.object.MapObject;
import vn.tcbs.library.math.MathUtils;

public class StockPrice {

	public static PutExec putExec(byte[] b) {
		PutExec ex = new PutExec();
		ex.setConfirmNo(MathUtils.bytesToInt(Arrays.copyOfRange(b, 0, 4)));
		ex.setStockNo(MathUtils.bytesToShort(Arrays.copyOfRange(b, 4, 6)));
		ex.setVol(MathUtils.bytesToInt(Arrays.copyOfRange(b, 6, 10)));
		ex.setPrice(MathUtils.bytesToInt(Arrays.copyOfRange(b, 10, 14)));
		ex.setBoard(new String(Arrays.copyOfRange(b, 14, 15)));
		return ex;
	}

	public static Le leFromBytes(byte[] b) {
		Le le = new Le();
		le.setStockNo(MathUtils.bytesToInt(Arrays.copyOfRange(b, 0, 4)));
		le.setPrice(MathUtils.bytesToInt(Arrays.copyOfRange(b, 4, 8)));
		le.setAccumulatedVol(MathUtils.bytesToInt(Arrays.copyOfRange(b, 8, 12)));
		le.setAccumulatedVal(MathUtils.getDouble(Arrays.copyOfRange(b, 12, 20)));
		le.setHighest(MathUtils.bytesToInt(Arrays.copyOfRange(b, 20, 24)));
		le.setLowest(MathUtils.bytesToInt(Arrays.copyOfRange(b, 24, 28)));
		le.setSince(MathUtils.bytesToInt(Arrays.copyOfRange(b, 28, 32)));
		return le;
	}

	public static Ls lsFromBytes(byte[] b) {
		Ls ls = new Ls();
		ls.setConfirmNo(MathUtils.bytesToInt(Arrays.copyOfRange(b, 0, 4)));
		ls.setStockNo(MathUtils.bytesToInt(Arrays.copyOfRange(b, 4, 8)));
		ls.setMatchedVol(MathUtils.getDouble(Arrays.copyOfRange(b, 8, 16)));
		ls.setPrice(MathUtils.bytesToInt(Arrays.copyOfRange(b, 16, 20)));
		ls.setSide(new String(Arrays.copyOfRange(b, 20, 21)));
		return ls;
	}

	public static HashMap<String, Object> fromBytes(byte[] b, List<MapObject> structure) {
		
		
		
		
		return null;
		
		/*
		security.setStockNo((int) MathUtils.bytesToShort(Arrays.copyOfRange(b,
				0, 2)));
		security.setStockSymbol(new String(Arrays.copyOfRange(b, 2, 10)));
		security.setStockType(new String(Arrays.copyOfRange(b, 10, 11)));
		security.setCeiling(MathUtils.bytesToInt(Arrays.copyOfRange(b, 11, 15)));
		security.setFloor(MathUtils.bytesToInt(Arrays.copyOfRange(b, 15, 19)));
		security.setBigLotValue(MathUtils.getDouble(Arrays.copyOfRange(b, 19,
				27)));
		security.setSecurityName(new String(Arrays.copyOfRange(b, 27, 52)));
		security.setSectorNo(new String(Arrays.copyOfRange(b, 52, 53)));
		security.setDesignated(new String(Arrays.copyOfRange(b, 53, 54)));
		security.setSuspension(new String(Arrays.copyOfRange(b, 54, 55)));
		security.setDelist(new String(Arrays.copyOfRange(b, 55, 56)));
		security.setHaltResumeFlag(new String(Arrays.copyOfRange(b, 56, 57)));
		security.setSplit(new String(Arrays.copyOfRange(b, 57, 58)));
		security.setBenefit(new String(Arrays.copyOfRange(b, 58, 59)));
		security.setMeeting(new String(Arrays.copyOfRange(b, 59, 60)));
		security.setNotice(new String(Arrays.copyOfRange(b, 60, 61)));
		security.setClientIDRequest(new String(Arrays.copyOfRange(b, 61, 62)));
		security.setCouponRate(MathUtils.bytesToShort(Arrays.copyOfRange(b, 62,
				64)));
		security.setIssueDate(new String(Arrays.copyOfRange(b, 64, 70)));
		security.setMatureDate(new String(Arrays.copyOfRange(b, 70, 76)));
		security.setAvrPrice(MathUtils.bytesToInt(Arrays.copyOfRange(b, 76, 80)));
		security.setParValue(MathUtils.bytesToShort(Arrays.copyOfRange(b, 80,
				82)));
		security.setIssueDate(new String(Arrays.copyOfRange(b, 82, 83)));
		security.setPriorClosePrice(MathUtils.bytesToInt(Arrays.copyOfRange(b,
				83, 87)));
		security.setPriorCloseDate(new String(Arrays.copyOfRange(b, 87, 93)));
		security.setPriorClosePrice(MathUtils.bytesToInt(Arrays.copyOfRange(b,
				93, 97)));
		security.setOpenPrice(MathUtils.bytesToInt(Arrays.copyOfRange(b, 97,
				101)));
		security.setLast(MathUtils.bytesToInt(Arrays.copyOfRange(b, 101, 105)));
		security.setLastVol(MathUtils.bytesToInt(Arrays
				.copyOfRange(b, 105, 109)));
		security.setLastVal(MathUtils.getDouble(Arrays.copyOfRange(b, 109, 117)));
		security.setHighest(MathUtils.bytesToInt(Arrays
				.copyOfRange(b, 117, 121)));
		security.setLowest(MathUtils.bytesToInt(Arrays.copyOfRange(b, 121, 125)));
		security.setTotalshare(MathUtils.getDouble(Arrays.copyOfRange(b, 125,
				133)));

		security.setTotalValue(MathUtils.getDouble(Arrays.copyOfRange(b, 133,
				141)));
		security.setAccumulateDeal(MathUtils.bytesToShort(Arrays.copyOfRange(b,
				141, 143)));
		security.setBigDeal(MathUtils.bytesToShort(Arrays.copyOfRange(b, 143,
				145)));
		security.setBigVol(MathUtils.bytesToInt(Arrays.copyOfRange(b, 145, 149)));
		security.setBigVal(MathUtils.getDouble(Arrays.copyOfRange(b, 149, 157)));
		security.setOddDeal(MathUtils.bytesToShort(Arrays.copyOfRange(b, 157,
				159)));
		security.setOddVol(MathUtils.bytesToInt(Arrays.copyOfRange(b, 159, 163)));
		security.setOddVal(MathUtils.getDouble(Arrays.copyOfRange(b, 163, 171)));
		security.setBest1Bid(MathUtils.bytesToInt(Arrays.copyOfRange(b, 171,
				175)));
		security.setBest1BidVolume(MathUtils.bytesToInt(Arrays.copyOfRange(b,
				175, 179)));
		security.setBest2Bid(MathUtils.bytesToInt(Arrays.copyOfRange(b, 179,
				183)));
		security.setBest2BidVolume(MathUtils.bytesToInt(Arrays.copyOfRange(b,
				183, 187)));
		security.setBest3Bid(MathUtils.bytesToInt(Arrays.copyOfRange(b, 187,
				191)));
		security.setBest3BidVolume(MathUtils.bytesToInt(Arrays.copyOfRange(b,
				191, 195)));
		security.setBest1Offer(MathUtils.bytesToInt(Arrays.copyOfRange(b, 195,
				199)));
		security.setBest1OfferVolume(MathUtils.bytesToInt(Arrays.copyOfRange(b,
				199, 203)));
		security.setBest2Offer(MathUtils.bytesToInt(Arrays.copyOfRange(b, 203,
				207)));
		security.setBest2OfferVolume(MathUtils.bytesToInt(Arrays.copyOfRange(b,
				207, 211)));
		security.setBest3Offer(MathUtils.bytesToInt(Arrays.copyOfRange(b, 211,
				215)));
		security.setBest3OfferVolume(MathUtils.bytesToInt(Arrays.copyOfRange(b,
				215, 219)));
		security.setLastUpdated(new Date());
	
		return security;
		*/
	}
}
