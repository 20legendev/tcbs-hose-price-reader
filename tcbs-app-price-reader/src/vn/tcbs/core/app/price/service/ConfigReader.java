package vn.tcbs.core.app.price.service;

import java.io.File;
import java.util.HashMap;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import vn.tcbs.library.file.FileUtils;

public class ConfigReader {

	private static HashMap<String, Configuration> configMap = new HashMap<String, Configuration>();

	public static Configuration getConfig(String filePath, Boolean isOut) {
		if (filePath == null) {
			return null;
		}
		if (isOut == true) {
			String path = FileUtils.getAbsolutePath();
			filePath = path + File.separator + filePath;
		}
		if (!configMap.containsKey(filePath)) {
			try {
				Configuration config = new PropertiesConfiguration(filePath);
				configMap.put(filePath, config);
			} catch (ConfigurationException e) {
				e.printStackTrace();
				return null;
			}
		}
		if (configMap.containsKey(filePath)) {
			return configMap.get(filePath);
		}
		return null;
	}
}
