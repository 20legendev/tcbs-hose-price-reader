package vn.tcbs.core.app.price.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.Configuration;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.MessageProperties;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;

public class QueueUtils {

	private static Configuration config;
	private static Channel channel;

	public static Channel getChannel() {
		if (config == null) {
			config = ConfigReader.getConfig("config/config.properties", true);
		}
		if (channel == null) {
			ConnectionFactory factory = new ConnectionFactory();
			String uri = config.getString("queue.uri");
			try {
				factory.setUri(uri);
				Connection conn = factory.newConnection();
				channel = conn.createChannel();
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return channel;
	}

	public static void writeToQueue(String exchangeName, byte[] body) {
		if (channel == null) {
			channel = getChannel();
		}
		try {
			channel.basicPublish(exchangeName, "",
					MessageProperties.PERSISTENT_TEXT_PLAIN, body);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void declareQueue(String queueName) {
		channel = channel == null ? QueueUtils.getChannel() : channel;
		String queueNameDb = queueName + ".db";
		String exchangeName = queueName + ".exchange";
		try {
			Map<String, Object> args = new HashMap<String, Object>();
			Integer ttl = config.getInteger("queue.msg.ttl", 120000);
			args.put("x-message-ttl", ttl);
			channel.exchangeDeclare(exchangeName, "direct", true);
			channel.queueDeclare(queueName, true, false, false, args);
			channel.queueDeclare(queueNameDb, true, false, false, null);
			channel.queueBind(queueName, exchangeName, "");
			channel.queueBind(queueNameDb, exchangeName, "");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void subcribeControlChange() throws IOException {
		String queueName = "tcbs.priceapp.control";
		channel.queueDeclare(queueName, true, false, false, null);
		QueueingConsumer consumer = new QueueingConsumer(channel);
		channel.basicConsume(queueName, false, consumer);

		QueueingConsumer.Delivery delivery;
		while (true) {
			try {
				delivery = consumer.nextDelivery();
				String message = new String(delivery.getBody());
				System.out.println(" [x] Received '" + message + "'");
			} catch (ShutdownSignalException e) {
				e.printStackTrace();
			} catch (ConsumerCancelledException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
}
