package vn.tcbs.core.app.price.service;

import java.io.File;
import java.util.concurrent.ConcurrentNavigableMap;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.DBMaker.Maker;

import vn.tcbs.library.file.FileUtils;

public class MapDatabase {
	private static DB db;
	private static ConcurrentNavigableMap<String, String> map;

	public static DB getConnection(String mode) {
		String filePath = FileUtils.getAbsolutePath() + File.separator
				+ "security.db";
		if (mode != null) {
			if (mode.equals("--nocached")) {
				File file = new File(filePath);
				file.delete();
			}
		}
		if (db == null) {
			try {
				File file = new File(filePath);
				Maker dbMarker = DBMaker.fileDB(file);
				dbMarker.closeOnJvmShutdown();
				dbMarker.encryptionEnable("kiennt10@techcombank.com.vn");
				db = dbMarker.make();
				return db;
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return db;
	}

	public static ConcurrentNavigableMap<String, String> getTreemap() {
		map = db.treeMap("fileReader");
		return map;
	}

	public static void set(String key, String value) {
		map.put(key, value);
		db.commit();
	}

	public static String get(String key, String defaultValue) {
		if (key == null)
			return defaultValue;
		map = getTreemap();
		String value = map.get(key);
		if (value == null) {
			value = defaultValue;
		}
		return value;
	}

}
