package vn.tcbs.core.app.price.service;

import java.io.File;

import org.apache.commons.configuration.Configuration;

import vn.tcbs.library.file.FileUtils;
import vn.tcbs.library.string.StringUtils;

public class DataPath {
	private static Configuration config = null;
	private static String dataLocation = null;

	public DataPath() {
		config = ConfigReader.getConfig("config/config.properties", true);
		dataLocation = config.getString("data.hose.location") + "/hostc_is/";
	}

	public String[] readDataPath() {
		String data = readData();
		String[] listFolder = StringUtils.splitString(data, 18);
		return listFolder;
	}

	public String[] getLastFolder() {
		String data = readData();
		String folder = data.substring(data.length() - 18);
		String[] obj = StringUtils.splitString(folder, 10);
		obj[1] = dataLocation + obj[1];
		return obj;
	}

	public String getDataLocation() {
		return config.getString("data.hose.location") + File.separator
				+ "hostc_is" + File.separator;
	}

	private String readData() {
		FileUtils fu = new FileUtils();
		return fu
				.readFile(config.getString("data.hose.location")
						+ File.separator + "hostc_is" + File.separator
						+ "datapath.map");
	}
}
