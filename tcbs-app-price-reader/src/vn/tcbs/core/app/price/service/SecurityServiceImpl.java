package vn.tcbs.core.app.price.service;

import vn.tcbs.core.app.price.dao.SecurityPriceDAOImpl;
import vn.tcbs.core.app.price.model.SecurityPrice;

public class SecurityServiceImpl implements SecurityService {
	private static SecurityPriceDAOImpl securityDao;

	public SecurityServiceImpl() {
		securityDao = new SecurityPriceDAOImpl();
	}

	public void addNew(SecurityPrice sp) {
		securityDao.addNew(sp);
	}

}
